#include "DatabaseAccess.h"
using std::string;

std::list<Album> m_albums;
std::list<User> m_users;
std::list<Picture> m_pic;
User userR;
Picture* pictureR;
int ret;
int albumID;

DatabaseAccess::DatabaseAccess()
{
	this->db = nullptr;
}

DatabaseAccess::~DatabaseAccess()
{
}

int getUserCallback(void* data, int argc, char** argv, char** azColName) {
	for (int i = 0; i < argc; i++) {
		if (string(azColName[i]) == "ID")
			userR.setId(atoi(argv[i]));
		else if (string(azColName[i]) == "NAME")
			userR.setName(string(argv[i]));
	}
	return 0;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	std::list<Album> userAlbums;
	std::list<Album> allAlbums = this->getAlbums();
	for (const auto& album : allAlbums)
	{
		if (album.getOwnerId() == user.getId()) {
			userAlbums.push_back(album);
		}
	}
	return userAlbums;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	std::string sqlStatement = "INSERT INTO ALBUMS (NAME, USER_ID, CREATION_DATE) VALUES ('" + album.getName() + "', " + std::to_string(album.getOwnerId()) + ", '" + album.getCreationDate() + "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string sqlStatement = "DELETE FROM ALBUMS WHERE NAME = '" + albumName + "' AND USER_ID = " + std::to_string(userId) + ";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	std::list<Album> allAlbums = this->getAlbums();
	for (const auto& album : allAlbums)
	{
		if (album.getName() == albumName && album.getOwnerId() == userId)
		{
			return true;
		}
	}
	return false;
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	std::list<Album> allAlbums = this->getAlbums();
	for (const auto& album : allAlbums)
	{
		if (album.getName() == albumName)
		{
			return album;
		}
	}
	throw MyException("No album with name " + albumName + " exists");
}

void DatabaseAccess::closeAlbum(Album& pAlbum)
{

}

void DatabaseAccess::printAlbums()
{
	std::list<Album> allAlbums = this->getAlbums();
	if (allAlbums.empty()) {
		throw MyException("There are no existing albums.");
	}
	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const Album& album : allAlbums) {
		std::cout << album.getCreationDate();
		std::cout << std::setw(5) << "* " << album;
	}
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	std::list<Album> allAlbums = this->getAlbums();
	bool flag = false;
	if (allAlbums.empty()) {
		throw MyException("There are no existing albums.");
	}

	for (const Album& album : allAlbums)
	{
		if (album.getName() == albumName)
		{
			flag = true;
			break;
		}
	}
	if (flag)
	{
		std::string sqlStatement = "INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES('" + picture.getName() + "', '" + picture.getPath() + "', '" + picture.getCreationDate() + "', " + std::to_string(this->getAlbumId(albumName)) + ");";
		char* errMessage = nullptr;
		int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			throw MyException(errMessage);
		}
	}
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	char* errMessage = nullptr;
	string sqlStatement = "DELETE FROM PICTURES WHERE NAME LIKE '" + pictureName + "';";
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		throw MyException(errMessage);
	}
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{

}


User DatabaseAccess::getTopTaggedUser()
{
	char* errMessage = nullptr;
	string sqlStatement = "SELECT USER_ID, USERS.NAME, COUNT(USER_ID) AS dupe_cnt FROM TAGS INNER JOIN USERS ON TAGS.USER_ID == USERS.ID GROUP BY USER_ID HAVING   COUNT(USER_ID) > 1 ORDER BY COUNT(USER_ID) DESC LIMIT 1;";
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), getUserCallback, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		throw MyException(errMessage);
	}
	return userR;
}

int getAllPicturesCallback(void* data, int argc, char** argv, char** azColName) {
	Picture pic;
	for (int i = 0; i < argc; i++) {
		if (string(azColName[i]) == "ID")
			pic.setId(atoi(argv[i]));
		else if (string(azColName[i]) == "NAME")
			pic.setName(string(argv[i]));
		else if (string(azColName[i]) == "LOCATION")
			pic.setPath(string(argv[i]));
		else if (string(azColName[i]) == "CREATION_DATE")
			pic.setCreationDate(string(argv[i]));
	}
	m_pic.push_back(pic);
	return 0;
}

int getPicturesTagsCallback(void* data, int argc, char** argv, char** azColName) {
	for (int i = 0; i < argc; i++)
		if (string(azColName[i]) == "USER_ID")
			pictureR->tagUser(atoi(argv[i]));
	return 0;
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	m_pic.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT PICTURE_ID, PICTURES.*, COUNT(PICTURE_ID) AS dupe_cnt FROM TAGS INNER JOIN PICTURES ON TAGS.PICTURE_ID == PICTURES.ID GROUP BY PICTURE_ID HAVING   COUNT(PICTURE_ID) > 1 ORDER BY COUNT(PICTURE_ID) DESC LIMIT 1;";
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), getAllPicturesCallback, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		throw MyException(errMessage);
	}
	Picture pic;
	for (auto& picture : m_pic) {
		pic = picture;
		break;
	}

	pictureR = &pic;
	sqlStatement = "SELECT * FROM TAGS WHERE PICTURE_ID == "; sqlStatement += std::to_string(pictureR->getId()) + ";";
	res = sqlite3_exec(this->db, sqlStatement.c_str(), getPicturesTagsCallback, nullptr, &errMessage);
	if (stat != SQLITE_OK) {
		throw MyException(errMessage);
	}
	return *pictureR;
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	m_pic.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT PICTURES.* FROM TAGS INNER JOIN PICTURES ON TAGS.PICTURE_ID == PICTURES.ID WHERE USER_ID == "; sqlStatement += std::to_string(user.getId()) + ";";
	int stat = sqlite3_exec(this->db, sqlStatement.c_str(), getAllPicturesCallback, nullptr, &errMessage);
	if (stat != SQLITE_OK) {
		throw MyException(errMessage);
	}
	for (auto& picture : m_pic) {
		sqlStatement = "SELECT * FROM TAGS WHERE PICTURE_ID == "; sqlStatement += std::to_string(picture.getId()) + ";";
		stat = sqlite3_exec(this->db, sqlStatement.c_str(), getPicturesTagsCallback, nullptr, &errMessage);
		if (stat != SQLITE_OK) {
			throw MyException(errMessage);
		}
	}
	return m_pic;
}

bool DatabaseAccess::open()
{
	std::string dbFileName = "galleryDB.sqlite";
	int res = sqlite3_open(dbFileName.c_str(), &(this->db));
	if (res != SQLITE_OK) {
		db = nullptr;
		throw MyException("Sqlite open faild");
	}
	return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(this->db);
	this->db = nullptr;
}

void DatabaseAccess::clear()
{
	char* errMessage = nullptr;
	int res;
	string sqlStatements[] = { "DELETE FROM TAGS;", "DELETE FROM PICTURES;", "DELETE FROM ALBUMS;", "DELETE FROM USERS;" };
	for (int i = 0; i < 4; i++) {
		res = sqlite3_exec(this->db, sqlStatements[i].c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK) {
			throw MyException(errMessage);
		}
	}
}

int callbackAllAlbums(void* data, int argc, char** argv, char** azColName)
{
	int userID = 0;
	string name;
	string creationDate;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "NAME")
		{
			name = argv[i];
		}
		else if (string(azColName[i]) == "CREATION_DATE")
		{
			creationDate = argv[i];
		}
		else if (string(azColName[i]) == "USER_ID")
		{
			userID = atoi(argv[i]);
		}
	}
	Album newAlbum = Album(userID, name, creationDate);
	m_albums.push_back(newAlbum);
	return 0;
}

int callbackAllUsers(void* data, int argc, char** argv, char** azColName)
{
	int id;
	string name;
	string creationDate;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "NAME")
		{
			name = argv[i];
		}
		else if (string(azColName[i]) == "ID")
		{
			id = atoi(argv[i]);
		}
	}
	User newAlbum = User(id, name);
	m_users.push_back(newAlbum);
	return 0;
}

auto DatabaseAccess::getAlbumIfExists(const std::string& albumName)
{
	std::list<Album> allAlbums = this->getAlbums();
	for (const auto& album : allAlbums)
	{
		if (album.getName() == albumName)
		{
			return album;
		}
	}
}

const std::list<Album> DatabaseAccess::getAlbums()
{
	m_albums.clear();
	char* sqlStatement = "SELECT * FROM ALBUMS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->db, sqlStatement, callbackAllAlbums, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	return m_albums;
}

void DatabaseAccess::printUsers()
{
	m_users.clear();
	char* sqlStatement = "SELECT * FROM USERS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->db, sqlStatement, callbackAllUsers, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	std::cout << "Users list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const auto& user : m_users) {
		std::cout << user << std::endl;
	}
}

void DatabaseAccess::createUser(User& user)
{
	std::string sqlStatement = "INSERT INTO USERS (ID, NAME) VALUES (" + std::to_string(user.getId()) + ", '" + user.getName() + "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
}

void DatabaseAccess::deleteUser(const User& user)
{
	std::string sqlStatement = "DELETE FROM USERS WHERE ID = " + std::to_string(user.getId()) + ";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
}

bool DatabaseAccess::doesUserExists(int userId)
{
	m_users.clear();
	char* sqlStatement = "SELECT * FROM USERS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->db, sqlStatement, callbackAllUsers, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	for (const auto& user : m_users) {
		if (user.getId() == userId)
		{
			return true;
		}
	}
	return false;
}

User DatabaseAccess::getUser(int userId)
{
	m_users.clear();
	char* sqlStatement = "SELECT * FROM USERS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->db, sqlStatement, callbackAllAlbums, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	for (const auto& user : m_users) {
		if (user.getId() == userId)
		{
			return user;
		}
	}
	throw MyException("The user is not exist");
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	int counter = 0;
	std::list<Album> allAlbums = this->getAlbums();
	for (const auto& album : allAlbums)
	{
		if (album.getOwnerId() == user.getId())
		{
			counter++;
		}
	}
	return counter;
}

int counterCallback(void* data, int argc, char** argv, char** azColName) {
	for (int i = 0; i < argc; i++)
		if (string(azColName[i]) == "counter")
			ret = atoi(argv[i]);
	return 0;
}

int albumIdCallback(void* data, int argc, char** argv, char** azColName) {
	for (int i = 0; i < argc; i++)
		if (string(azColName[i]) == "ID")
			albumID = atoi(argv[i]);
	return 0;
}

int DatabaseAccess::getAlbumId(string name)
{
	char* errMessage = nullptr;
	string sqlStatement = "SELECT ID FROM ALBUMS WHERE NAME LIKE '" + name + "';";
	int stat = sqlite3_exec(this->db, sqlStatement.c_str(), albumIdCallback, nullptr, &errMessage);
	if (stat != SQLITE_OK) {
		throw MyException(errMessage);
	}
	return albumID;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	char* errMessage = nullptr;
	string sqlStatement = "SELECT COUNT(DISTINCT ALBUM_ID) AS counter FROM TAGS INNER JOIN PICTURES ON TAGS.PICTURE_ID == PICTURES.ID WHERE USER_ID == "; sqlStatement += std::to_string(user.getId()) + ";";
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), counterCallback, nullptr, &errMessage);
	if (stat != SQLITE_OK)
		throw MyException(errMessage);
	return ret;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	char* errMessage = nullptr;
	string sqlStatement = "SELECT COUNT(PICTURE_ID) AS counter FROM TAGS WHERE USER_ID == "; sqlStatement += std::to_string(user.getId());
	int stat = sqlite3_exec(this->db, sqlStatement.c_str(), counterCallback, nullptr, &errMessage);
	if (stat != SQLITE_OK) {
		throw MyException(errMessage);
	}
	return ret;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int tags = countAlbumsTaggedOfUser(user);
	if (0 == tags)
		return 0;
	return countTagsOfUser(user) / tags;
}